package com.example.insuranceassignment

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.text.TextUtils
import android.util.Patterns
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

class MainActivity : AppCompatActivity() {
    lateinit var tiEmail: TextInputLayout
    lateinit var tiPass: TextInputLayout
    lateinit var etEmail: TextInputEditText
    lateinit var etPass: TextInputEditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        etEmail = findViewById(R.id.et_email)
        etPass = findViewById(R.id.et_pass)
        tiEmail = findViewById(R.id.text_ip_email)
        tiPass = findViewById(R.id.text_ip_pass)

    }

    fun onLogin(view: View) {
        if (!loginValidate()) {
            Toast.makeText(this, getString(R.string.login_toast), Toast.LENGTH_SHORT).show()
            startActivity(Intent(this, BaseActivity::class.java))
        }


    }


    private fun loginValidate(): Boolean {
        var isError = false
        val email = etEmail.text.toString()
        val pass = etPass.text.toString()
        if (TextUtils.isEmpty(email) || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            isError = true
            tiEmail.isErrorEnabled = true
            tiEmail.error = getString(R.string.login_emailerror)
        } else {
            isError = false
            tiEmail.isErrorEnabled = false
            tiEmail.error = ""
        }
        if (TextUtils.isEmpty(pass) || pass.length < 3) {
            isError = true
            tiPass.isErrorEnabled = true
            tiPass.error = getString(R.string.login_passerror)
        }
        return isError

    }


}

