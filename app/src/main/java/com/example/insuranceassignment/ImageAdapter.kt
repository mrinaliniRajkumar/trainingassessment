package com.example.insuranceassignment

import android.content.Context
import android.graphics.drawable.Drawable
import android.media.Image
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView


class ImageAdapter(context : Context): RecyclerView.Adapter<ImageAdapter.ViewHolder>() {
    val imagelist = arrayListOf(R.drawable.idcard_1,R.drawable.idcard_2,R.drawable.idcard_3)
    val mContext = context

    class ViewHolder( view: View) : RecyclerView.ViewHolder(view){
        val placeImage = view.findViewById<ImageView>(R.id.id_imageview)

        fun setData(image: Drawable){
            placeImage.setImageDrawable(image)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.viewpager_image_item,parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        mContext.getDrawable(imagelist[position])?.let { holder.setData(it) }
    }

    override fun getItemCount(): Int {
        return imagelist.size
    }
}