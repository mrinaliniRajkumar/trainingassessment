package com.example.insuranceassignment

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.fragment.app.Fragment
import com.example.insuranceassignment.fragment.*
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationBarView

class BaseActivity : AppCompatActivity() {

    lateinit var bottomNavigationView:BottomNavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)
    }

    override fun onResume() {

        super.onResume()
        setUpView()

    }

    private fun setUpView() {
        bottomNavigationView = findViewById(R.id.bottom_navbar)
        supportFragmentManager.beginTransaction().replace(R.id.baseframelayout, ProfileFragment())
            .commit()
        bottomNavigationView.setOnItemSelectedListener { item ->
            lateinit var fragment: Fragment
            when (item.itemId) {
                R.id.navbar_more -> {
                    fragment = MoreFragment()
                }
                R.id.navbar_quotes -> {
                    fragment = QuotesFragment()
                }
                R.id.navbar_policies -> {
                    fragment = PoliciesFragment()
                }
                R.id.navbar_dashboard -> {
                    fragment = DashboardFragment()
                }
                R.id.navbar_profile -> {
                    fragment = ProfileFragment()
                }
            }

            supportFragmentManager.beginTransaction().replace(R.id.baseframelayout, fragment)
                .commit()


            true
        }

    }

}