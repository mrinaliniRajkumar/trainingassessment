package com.example.insuranceassignment.fragment

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.example.insuranceassignment.BaseActivity
import com.example.insuranceassignment.ImageAdapter
import com.example.insuranceassignment.R
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class DashboardFragment : Fragment() {
    lateinit var viewPager: ViewPager2
    lateinit var mActivity: BaseActivity


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        requireActivity().title = getString(R.string.dash_title)
        setHasOptionsMenu(true)
        val view = inflater.inflate(R.layout.fragment_dashboard, container, false)
        initializeView(view)
        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_profile,menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.account_menu->{
                Toast.makeText(requireActivity(), getString(R.string.menu_toast), Toast.LENGTH_SHORT).show()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }


    private fun initializeView(view: View) {
        mActivity = requireActivity() as BaseActivity
       viewPager = view.findViewById(R.id.id_viewpager)
        viewPager.adapter = ImageAdapter(mActivity)
        val tabLayout = view.findViewById<TabLayout>(R.id.viewpager_tablayout)
        TabLayoutMediator(tabLayout,viewPager){tab,position -> }.attach()

    }
}